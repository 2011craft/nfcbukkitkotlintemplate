plugins {
    kotlin("jvm") version "1.9.0"
    application
}

group = "org.craft11"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    implementation(files("lib/bukkit.jar", "lib/bukkit-permissions.jar", "lib/bukkit-permissionsex.jar"))
    implementation(files("lib/spout.jar"))
    implementation(files("lib/libNFC.jar"))
}



tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
}

application {
    mainClass.set("MainKt")
}